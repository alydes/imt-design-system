# Desktop

* Max-width: 2560px

# Tablet

* Max-width: 1024px

# Tablet/Mobile Landscape

* Max-width: 768px

# Mobile

* Max-width: 480px
* Min-width: 320px

## Notes
* While there are specified breakpoints, most content should be fluid between each breakpoint. More details will be provided in the specifications for each project.
* A min-width is also provided as we will not need to design for screens smaller than 320px; designers should keep this minimum breakpoint in mind when creating mockups.