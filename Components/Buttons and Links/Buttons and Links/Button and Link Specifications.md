# Button Redlines
* https://e9qm7v.axshare.com
    * Unfortunately, Axure Cloud does not allow users to view redelines unless the specific user has been invited to view the document. All of the development team has access, however, if you would like full access, please email me at alysondesmarais4@gmail.com.

# Specifications
**Font**
* 19px, Open Sans, bold, sentence case

**Border**
* Border radius 5px
* Border weight (for secondary buttons only) 2px

**Height**
* 48px (57px on homepage)
    
**Width**
* Buttons have no set width but have a minimum width of 100px
* From 479px to 320px main buttons span the width of the screen

**Padding** 
* 16px around text

# States
* Each state has a primary button (filled), a secondary button (border only), and a link. 

## Default
* A button and link in its resting state. 

![regular](/uploads/352953ab7ebf4ea80e6fd76f4446f848/regular.png)

## Hover
* These buttons and link will lift off the page and form a shadow/thicker border/underline when the user hovers over the component with their cursor. This will reiterate the button’s presence and will draw the user’s eye towards the button. 

![hover](/uploads/0caa3115c54a466ac219610ec04d3e33/hover.png)

## Active

* Buttons and links are active when they have been clicked on. This allows users to understand if an action has already been completed. In this state, no additional actions should occur if buttons are clicked on repeatedly.  

![active](/uploads/98f757a718d53c2d822f218a05e40e03/active.png)

## Disabled

* These are buttons and links that cannot be clicked on and are not accessible. For example, these buttons would be used when a form or input has not been completed properly and the user cannot proceed without corrections.

![disabled](/uploads/90a08ce700b28c4ddff9f53336115a35/disabled.png)

## Disabled State with Pop-up
* When a disabled button is clicked, a red pop-up should appear that explains to the user why clicking the disabled button is not allowing them to proceed.
* This is also helpful for those with visual impairments as screen readers can read the text in the pop-up and describe what is wrong to the user. 

![disabled_w_popup](/uploads/f99c7b6c3e1dbea5b1921658ebada673/disabled_w_popup.png)