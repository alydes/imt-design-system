# Field Redlines
* https://32fkvy.axshare.com
    * Unfortunately, Axure Cloud does not allow users to view redelines unless the specific user has been invited to view the document. All of the development team has access, however, if you would like full access, please email me at alysondesmarais4@gmail.com.

# Specifications

**The Basics**
* On focus, on mobile, the fields move based on the phones native functionality.
* A user is able to add spaces after entering information into an input field. 

**Height**
* 48px (57px on homepage)
    
**Width**
* Input widths correlate with the width of the input text (ex: smaller fields for age, date, trip cost). 
* On mobile, the largest input fields expand to the full width of the screen.
    * This does not occur with smaller fields: age, age of other travelers, and total trip cost.

**Padding** 
* 16px of padding to the left of the placeholder text/calendar icon and 16px of padding to the right of the error icon
* 17px of padding to the left of help text and error message for the resting and completed state and 18px of padding to the left of help text and error message for the focus and error state.
	* The numbers may look off, but everything should line up. The measurements take into account the fields border thickness 
* 10px of padding between the label and input field and between the input field and help text  


# States 

## Resting
* Black outline with placeholder text
* This is the input field's resting state.

![Resting_State](/uploads/3550bf00c588c40550a317e22282e156/Resting_State.png)

## Focus
* Blue outline with blue shadow and placeholder text
* This state occurs when a user clicks or tabs into a field.

![Focus_State](/uploads/ca773b7b6c7dfe37747fcede633b3573/Focus_State.png)

## Completed
* Black outline with black text
* This state occurs after a user has entered information into the field and clicks out of the field.
	* The entered information must have also passed any validations. 

![Completed_State](/uploads/c6f662f5e6785bb983426bf00de885ee/Completed_State.png)
    
## Error
* Red outline, red error symbol, and red error message
* The error state occurs when a user has entered invaid information.
* The error state is shown as soon a user has clicked or tabbed out of the field with the invalid information. 

![Error_State](/uploads/ebb8ba721a858e488729874da6dacb11/Error_State.png)


