![IMT_green](/uploads/1817edbb2e9bd5ae5e085ddfa33f95aa/IMT_green.png)
## Primary Green #77BB33
* Navigation bar
* Graphics color
* Accent color

<br>

<br>

<br>

![error_red](/uploads/e6a7205c61679d8ec46612c5785e0fc2/error_red.png)
## Error Red #EB0000
* Error messages
* Error symbol in input field
* Error input field border

<br>

<br>

<br>

![IMT_blue](/uploads/5cdafe42ebd7b491f93917503e87df45/IMT_blue.png)
## Primary Blue #0099CC
* Graphics color
* Accent color

<br>

<br>

<br>

![button_blue](/uploads/704c5263d20727123aa26c5fa6d03d31/button_blue.png)
## Button Blue #0280A7
* Button background
* Link color

<br>

<br>

<br>

![active_blue](/uploads/1ab6f88d8b4014c3a0dbb5bdec1ff0c0/active_blue.png)
## Active Button Blue #016686
* Button background in active state
* Link color in active state

<br>

<br>

<br>

![light_text](/uploads/e552bda9146ff79e7396546b31d5e1b5/light_text.png)
## Light Text #485D6A
* Secondary text/text of lesser importance

<br>

<br>

<br>

![light_gray](/uploads/b67d3196ba736ada15643406b491a4e8/light_gray.png)
## Light Gray #DBDBDB 
* Disabled button background

<br>

<br>

<br>

![medium_gray](/uploads/bf84534c142150a3bca17d9b3eba59a3/medium_gray.png)
## Medium Gray #A6A6A6 
* Disabled link color
* Disabled secondary button border/text color

<br>

<br>

<br>

![dark_gray](/uploads/2a690f3d34fc43cb56884108d7e3a817/dark_gray.png)
## Dark Gray #757575
* Placeholder text 
* Text on disabled primary buttons

<br>

<br>

<br>

![dark_text](/uploads/11964d2fd4dfe7682e06ad4d88ab0951/dark_text.png)
## Dark Text #333333
* Body text
* Footer background